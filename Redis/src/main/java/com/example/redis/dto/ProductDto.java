package com.example.redis.dto;

import jakarta.persistence.Column;
import lombok.Data;

@Data
public class ProductDto {
    private long id;
    private long productID;
    private String productName;
    private int productQuantity;
    private int productPrice;
}

