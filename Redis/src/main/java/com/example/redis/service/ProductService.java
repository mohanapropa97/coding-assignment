package com.example.redis.service;

import com.example.redis.dto.ProductDto;
import com.example.redis.entity.Product;
import com.example.redis.repository.ProductRepository;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
@Data
@Service
public class ProductService {
    private final ProductRepository productRepository;

    public String saveProduct(ProductDto productDto) {
        if (productRepository.existsByProductName(productDto.getProductID()))
            return "Product already Exist with this EmployeeID";
        else {
            Product product = dtoToEntity(productDto);
            productRepository.save(product);
            return "Saved Successfully";
        }
    }
    private Product dtoToEntity(ProductDto productDto){
        Product product= new Product();
        BeanUtils.copyProperties(productDto,product);
        return product;
    }

}
