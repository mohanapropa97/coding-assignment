# Program Description
This Java Spring Boot application is designed to manage user data, including information about users such as first name, last name, and address. Two types of users are supported: Parent and Child. A Child must belong to a Parent, and a Child cannot have an address.

The application provides APIs for basic CRUD operations:

- ****Create User Data:**** Endpoint to add new user data to the database.
- ****Delete User Data:**** Endpoint to remove user data from the database.
- ****Update User Data:**** Endpoint to modify existing user data in the database.

# Environmental Dependency
## 1. Technology Used
- Java
- Spring Boot
- Spring Data jpa
- MySQL Database
- Api testing tool

## 2. Dependency Used
It is needed to create the project with spring initializr. After that the following dependencies 
- Spring Boot
- lombok
- Spring web
- Spring Data JPA
- MySQL driver
- Maven (for dependency management)
- any API testing tool.

## 3. Software Version 
 - Intelij IDEA 2023.2.5
 - MySQL workbench 8.0.35
 - Postman
 
# Configure Spring Boot for Database

****File destination:****```src/main/resources/application.properties```

```bash
#Database configuration for MySQL
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/user?createDatabaseIfNotExist=true&allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=1234

#JPA hibernate configuration
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL8Dialect
spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
```
# Access the API:

- Open any API testing tool(I've used Postman).
- Use the following base URL: http://localhost:8080/user

# API Endpoints

#### 1. Create User Data

- ****End point:**** ```localhost:8080/user/saveParent```
- ****Request Body:****
``` json
{
    "parentID": 103,
    "parentFirstName": "Bijoy",
    "parentLastName": "Ghosh",
    "addressDto": {
        "street": "Dhaka",
        "city": "Dhaka",
        "state": "Dhanmondi",
        "zip": "1205"
    },
    "childDto": {
        "childFirstName": "Akash",
        "childLastName": "Ghosh"
    }
}
```
#### 2. Fetch All User

- ****End point:**** ```localhost:8080/user/getAllUsers```
- ****Request Body:****
``` json
{
        "id": 2,
        "parentID": 101,
        "parentFirstName": "Propa",
        "parentLastName": "Banik",
        "addressDto": {
            "id": 2,
            "street": "Dhaka",
            "city": "Dhaka",
            "state": "Mirpur-12",
            "zip": "1216"
        },
        "childDto": {
            "id": 2,
            "childFirstName": "Akash",
            "childLastName": "Ghosh"
        }
    },
    {
        "id": 4,
        "parentID": 103,
        "parentFirstName": "Bijoy",
        "parentLastName": "Paul",
        "addressDto": {
            "id": 4,
            "street": "Dhaka",
            "city": "Dhaka",
            "state": "Dhanmondi",
            "zip": "1205"
        },
        "childDto": {
            "id": 4,
            "childFirstName": "Akash",
            "childLastName": "Paul"
        }
    }
```
#### 3. Delete User Data

- ****End point:**** ```localhost:8080/user/deleteById/{parentID}```

#### 4. Update User Data

- ****End point:**** ```localhost:8080/user/updateParent/{parentID}```
- ****Request Body:****
``` json
{
    "parentID": 103,
    "parentFirstName": "Bijoy",
    "parentLastName": "Ghosh",
    "addressDto": {
        "street": "Dhaka",
        "city": "Dhaka",
        "state": "mirpur-6",
        "zip": "1205"
    },
    "childDto": {
        "childFirstName": "Ridhi",
        "childLastName": "Paul"
    }
}
```

# Database Schema
The database schema consists of tables for parent, child and address.
Temporary tables of parent_address and parent_child have been created to establisha relationship between parents, children and address.

