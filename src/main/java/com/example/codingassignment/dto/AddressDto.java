package com.example.codingassignment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddressDto {
    private long id;
    private String street;
    private String city;
    private String state;
    private String zip;
}
