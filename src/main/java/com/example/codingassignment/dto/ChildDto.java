package com.example.codingassignment.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ChildDto {
    private long id;
    private String childFirstName;
    private String childLastName;
}
