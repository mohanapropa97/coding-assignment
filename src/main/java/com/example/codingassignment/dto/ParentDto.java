package com.example.codingassignment.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ParentDto {
    private long id;
    private long parentID;
    private String parentFirstName;
    private String parentLastName;
    private AddressDto addressDto;
    private ChildDto childDto;
}
