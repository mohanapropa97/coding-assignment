package com.example.codingassignment.service;
import com.example.codingassignment.dto.AddressDto;
import com.example.codingassignment.dto.ChildDto;
import com.example.codingassignment.dto.ParentDto;
import com.example.codingassignment.entity.Address;
import com.example.codingassignment.entity.Child;
import com.example.codingassignment.entity.Parent;
import com.example.codingassignment.repository.AddressRepository;
import com.example.codingassignment.repository.ChildRepository;
import com.example.codingassignment.repository.ParentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class UserService {

    public final ParentRepository parentRepository;
    public final AddressRepository addressRepository;
    public final ChildRepository childRepository;

    public String saveParent(ParentDto parentDto) {
        if (parentDto == null) {
            return "ParentDto is null";
        } else if (parentRepository.existsByParentID(parentDto.getParentID()))
            return "Employee already Exist with this EmployeeID";

        else {
            Parent parent = dtoToEntity(parentDto);
            parentRepository.save(parent);
            return "Saved Successfully";
        }
    }

    public List<ParentDto> getAllUsers() {
        List<Parent> parentList = parentRepository.findAll();
        List<ParentDto> parentDtoList = new ArrayList<>();
        for (Parent parent : parentList) {
            ParentDto parentDto = entityToDto(parent);
            parentDtoList.add(parentDto);
        }
        return parentDtoList;
    }

    public String  deleteParent(Long parentID) {
        Optional<Parent> optionalParent = parentRepository.findByParentID(parentID);
        if (optionalParent.isPresent()) {
            Parent parent = optionalParent.get();

            if (parent.getAddress() != null) {
                addressRepository.delete(parent.getAddress());
            }

            if (parent.getChild() != null) {
                childRepository.delete(parent.getChild());
            }
            parentRepository.delete(parent);
            return "Parent with ID " + parentID + " is deleted.";
        }
        else {
            return "Parent with ID " + parentID + " not found";
        }

    }
    public String updateUser(Long parentID, ParentDto newParentDto) {
        Optional<Parent> optionalParent = parentRepository.findByParentID(parentID);
        if (optionalParent.isPresent()) {
            Parent parent = optionalParent.get();

            if (newParentDto.getParentFirstName() != null) {
                parent.setParentFirstName(newParentDto.getParentFirstName());
            }
            if (newParentDto.getParentLastName() != null) {
                parent.setParentLastName(newParentDto.getParentLastName());
            }

            if (newParentDto.getAddressDto() != null) {
                if (parent.getAddress() == null) {
                    parent.setAddress(new Address());
                }
                AddressDto addressDto = newParentDto.getAddressDto();
                Address address = parent.getAddress();
                address.setStreet(addressDto.getStreet());
                address.setCity(addressDto.getCity());
                address.setState(addressDto.getState());
                address.setZip(addressDto.getZip());
            }
            if (newParentDto.getChildDto() != null) {
                if (parent.getChild() == null) {
                    parent.setChild(new Child());
                }

                ChildDto childDto = newParentDto.getChildDto();
                Child child = parent.getChild();
                child.setChildFirstName(childDto.getChildFirstName());
                child.setChildLastName(childDto.getChildLastName());
            }
            parentRepository.save(parent);
            entityToDto(parent);
            return "Parent with ID " + parentID + " is updated.";
        }
        else {
            return "Parent with ID " + parentID + " not found";
        }
    }

    private Parent dtoToEntity(ParentDto parentDto){
        Parent parent= new Parent();
        BeanUtils.copyProperties(parentDto,parent);

        if (parentDto.getAddressDto() != null) {
            Address address = new Address();
            BeanUtils.copyProperties(parentDto.getAddressDto(), address);
            parent.setAddress(address);
        }

        if (parentDto.getChildDto() != null) {
            Child child = new Child();
            BeanUtils.copyProperties(parentDto.getChildDto(), child);
            parent.setChild(child);
        }

        return parent;
    }

    private ParentDto entityToDto(Parent parent){
        ParentDto parentDto = new ParentDto();
        BeanUtils.copyProperties(parent, parentDto);
        if (parent.getAddress() != null) {
            AddressDto addressDto = new AddressDto();
            BeanUtils.copyProperties(parent.getAddress(), addressDto);
            parentDto.setAddressDto(addressDto);
        }
        if (parent.getChild() != null) {
            ChildDto childDto = new ChildDto();
            BeanUtils.copyProperties(parent.getChild(), childDto);
            parentDto.setChildDto(childDto);
        }
        return parentDto;
    }
}
