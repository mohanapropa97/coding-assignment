package com.example.codingassignment.controller;


import com.example.codingassignment.dto.ParentDto;
import com.example.codingassignment.entity.Parent;

import com.example.codingassignment.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;


    @PostMapping("/saveParent")
    public ResponseEntity<?> saveParent(@RequestBody ParentDto parentDto) {
        try {
            String message = userService.saveParent(parentDto);
            return new ResponseEntity<>(message, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getAllUsers")
    public List<ParentDto> getAllUsers() {
        return userService.getAllUsers();
    }
    @DeleteMapping("/deleteById/{parentID}")
    public ResponseEntity<?> deleteParent(@PathVariable Long parentID) {
        String message = userService.deleteParent(parentID);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PutMapping("/updateParent/{parentID}")
    public ResponseEntity<?> updateUser(@PathVariable Long parentID, @RequestBody ParentDto newParentDto) {
        String message=userService.updateUser(parentID,newParentDto);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

}
