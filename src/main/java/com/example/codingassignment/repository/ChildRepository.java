package com.example.codingassignment.repository;

import com.example.codingassignment.entity.Child;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChildRepository extends JpaRepository<Child, Long> {
}
