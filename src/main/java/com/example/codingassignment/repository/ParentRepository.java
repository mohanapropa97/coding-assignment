package com.example.codingassignment.repository;

import com.example.codingassignment.entity.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ParentRepository extends JpaRepository<Parent, Long> {
    Boolean existsByParentID(Long parentID);

    Optional<Parent> findByParentID(Long parentID);
    Parent deleteByParentID(Long parentID);

}
